=========
Tutorials
=========

The following tutorials describe some use cases of platform migrator in moving
software from one system to another.

Migrating from conda to conda
-----------------------------

This is the simplest and most straightforward use case of platform migrator.
The requirements for this use case are:

Software to be migrated
    - The software must be in an executable format (either binary or source
      code)
    - There should be a set of tests that can be run to determine whether the
      software works as intended or not.

Base System
    - The base system must contain a dedicated conda environment for the
      software. This may be the conda root environment.
    - Access to internet

Target System
    - The target system must be a system where the user can install or has
      already installed conda. The tutorial will cover installation of conda.
    - Access to internet

Preparing the target system
+++++++++++++++++++++++++++

First, install platform migrator on the target system as per the instructions
in :ref:`install`. There is no need for any configuration in this use case.

Next, go to `Anaconda installation page <https://conda.io/docs/user-guide/install/index.html>`_
and install the version of conda for your target system. You can skip this if
you already have conda installed on the target system.

.. _transfer:

Transferring the software
+++++++++++++++++++++++++

There are 2 ways to do this. The end result of both the ways is the same and
has no impact on the overall success or failure of the migration. Following
any one of the two options below is sufficient to attempt an migration.

Option 1. Over a local network
******************************

On the target system, start the platform migrator server by executing ::

    platform-migrator server --host 0.0.0.0 start

Here, platform migrator runs in server mode and starts a daemon process, which
is a simple HTTP server which listens on all IP addresses and port 9001. If you
wish to listen only on a particular IP address and port, use the ``--host`` and
``--port`` options to change them. Regardless, you should note the IP address of
your target system. On Linux or Mac, this can be obtained by running ``ifconfig``
in a terminal and using the IP address on an interface other than the loopback
interface, ``lo``. On Windows, this can be obtained by running cmd and typing
in ``ipconfig``. Note the IP address for use on the base system.

Now, on the base system, download a python script from the target system by
running ::

    curl http://<IP-address-of-target-system>:9001/migrate > script.py

If curl is not available, activate the conda root environment on the base
system and run the following code in a python interpreter ::

    import requests
    resp = requests.get('http://<IP-address-of-target-system>:9001/migrate')
    with open('script.py', 'w') as fp:
        fp.write(resp.text)
    exit()

The script is a Python script generated on the target system and it contains
some procedures to probe the conda environment for the packages installed and
to transfer the software and settings over to the target system.

Make sure that the conda executable is on the PATH of the shell. If it is not,
run ``export PATH=$PATH:/path/to/conda/bin`` to add it to the PATH variable.
Now, activate the conda environment that is used by the software to be
migrated and run ``python script.py`` and follow the prompts on screen. The
script will ask you to enter the name of the software and the directory under
which the source code is saved, if everything goes well. When prompted
regarding saving a zip file for emailing later, enter ``n``.

Option 2. With email, scp or other manual methods
*************************************************

This an alternate way to transfer software, in case network connection cannot
be setup between the base and the target system. In this case, the server
startup is not required on the target system and server mode is not involved.

To do this, first install platform migrator on the base system, by following
the installation steps. After that, execute ``platform-migrator pack``. This
will run the same script as above, but when prompted to save a zip file say
``y``. A zip file will be saved in the current directory. This can be copied
to the target system using any method like email, scp, ftp or manual copy,
and unpacked there using ``platform-migrator unpack``.

Installing and testing on the target system
+++++++++++++++++++++++++++++++++++++++++++

Open up a text editor and create a test configuration for the software. A
minimal test configuration would look like ::

    [<software-name>]
    package_managers = conda
    output_dir = <directory-where-you-want-to-save-the-software>
    tests = <test-name0>, <test-name1>

    [test_<test-name0>]
    exec = <command-to-run-test0>

    [test_<test-name1>]
    exec = <command-to-run-test1>

The ``<software-name>`` should be replaced by the name used on the base system.
The ``output_dir`` should be an absolute path to the directory where the
software must be saved post migration. The ``tests`` option contains a comma
separated list of tests that should be run on the software. For each test in
this list, there must be a corresponding ``test_<name>`` section which contains
a ``exec`` option containing the command to execute. This command must return 0
on success and any other value on failure. The command will be run from inside
the directory in which software is installed and can use paths relative to that.

So, for example, a test called ``foo`` executes a script called ``tests/foo.sh``
inside the software, the test configuration will look like ::

    [<software-name>]
    package_managers = conda
    output_dir = /tmp
    tests = foo

    [test_foo]
    exec = bash tests/foo.sh

See :doc:`test-config-file` for the complete documentation on this.

Once the tests have been configured, execute ::

    platform-migrator migrate <software-name> <tests-config-file-name>

Now, platform migrator will run in ``migrate`` mode and try to install the
software along with any required dependencies.

Platform migrator will create a new conda environment called <software-name>
and run the tests for the software there. If the tests pass, the software will
be copied into the output directory. Otherwise, the error message from the tests
will be displayed on screen and the environment will be destroyed.

Installing without any tests
++++++++++++++++++++++++++++

In case there are not tests available, you use the ``--skip-tests`` option and
platform migrator will create a conda environment and copy the files to the output
directory. A configuration file is still required but it need not contain any
tests.

.. _config_pkg_mgr:

Configuring an alternate package manager
----------------------------------------

This tutorial walks you through another essential part of platform migrator,
which is configuring it to use a package manage other than conda. These
additional package managers can be added to the default file in
``~/.platform-migrator/config/`` on POSIX OSes. For Windows, replace ``~``
with the user's home directory.

Open up the ``package-managers.ini`` file in a text editor. The file contains
the documentation of the file along with some sample package managers. These
package managers are supported out of the box on Linux distros, but not on Windows
and Mac. On Windows, you will probably need Cygwin or some other POSIX shell
utility for these to be actually useful.

To add a new package manager to the file, create a new section. The section name
will be used in the list of package managers in the test configuration file during
the migration process, so keep the indicative of the package manager. Typically,
this would be the name of the executable of the package manager.

In the section, add the following options and their values as described:

:name: The actual name. This is only for your benefit and not used by platform
       migrator.
:exec: The executable of the package manager used on the command line
:install: The command used to install a package where the package name will be
          at the end of the command.
:install_version: The command used to install a specific version of package.
:search: The command used to search the package manager for packages. This will
         typically require some further text manipulation since most package
         managers tend to provide human readable output rather than machine
         parseable output.
:result_fmt: A regular expression describing the results of the search.

Read the :doc:`package-manager-config-file` for details on how the values of
each option should be written. An example of :ref:`conf_apt_get` is also
provided in the :doc:`case-studies` section.

If you do not wish to edit the default file, you can save your configuration in
a separate .ini file in the ``~/.platform-migrator`` directory and use it in
addition to the default file by using the
``--pck-mgr-configs ~/.platform_migrator/<filename>.ini`` option during
migration.

Migrating from conda to a different package manager(s)
------------------------------------------------------

This is a more complex migration which requires some input and knowledge on
the user's end about the software being migrated.
The requirements for this use case are:

Software to be migrated
    - The software must be in an executable format (either binary or source
      code)
    - There should be a set of tests that can be run to determine whether the
      software works as intended or not.

Base System
    - The base system must contain a dedicated conda environment for the
      software. This may be the conda root environment.
    - Access to internet

Target System
    - The target system must contain one or more package managers and platform
      migrator must be configured to use them.
    - Access to internet

Preparing the target system
+++++++++++++++++++++++++++

First, install platform migrator on the target system as per the instructions
in :ref:`install`. Follow the tutorial in :ref:`config_pkg_mgr` and configure a
new package manager. Further instructions will assume that the package manager
has been configured in a separate file called ``~/.platform-migrator/site-pkg-mgrs.ini``

Transferring the software
+++++++++++++++++++++++++

This part is exactly the same as in the migration from conda to conda.
Please refer the :ref:`transfer`  section for the documentation.

Installing and testing on the target system
+++++++++++++++++++++++++++++++++++++++++++

Open up a text editor and create a test configuration for the software. A
minimal test configuration would look like ::

    [<software-name>]
    package_managers = <site-package-manager0>, <site-package-manager1>
    output_dir = <directory-where-you-want-to-save-the-software>
    tests = <test-name0>, <test-name1>

    [test_<test-name0>]
    exec = <command-to-run-test0>

    [test_<test-name1>]
    exec = <command-to-run-test1>

The ``package_managers`` option is a comma-separated list of the package
managers that will be used by platform-migrator for installing the
required packages. The package managers are used in the order entered, so
``<site-package-manager0>`` is probed before probing ``<site-package-manager1>``.
The ``<software-name>`` should be replaced by the name used on the base system.
The ``output_dir`` should be an absolute path to the directory where the
software must be saved post migration. The ``tests`` option contains a comma
separated list of tests that should be run on the software. For each test in
this list, there must be a corresponding ``test_<name>`` section which contains
a ``exec`` option containing the command to execute. This command must return 0
on success and any other value on failure. The command will be run from inside
the directory in which software is installed and can use paths relative to that.

So, for example, a test called ``foo`` executes a script called ``tests/foo.sh``
inside the software and the site runs Archlinux with pip installed on it, the
test configuration will look like ::

    [<software-name>]
    package_managers = pacman, pip
    output_dir = /tmp
    tests = foo

    [test_foo]
    exec = bash tests/foo.sh

Once the tests have been configured, execute ::

    platform-migrator migrate --pck-mgr-configs site-pkg-mgrs.ini <software-name> <tests-config-file-name>

Now, platform migrator will run in ``migrate`` mode and try to install the
software along with any required dependencies.

Platform migrator will prompt the user for each package that needs to be
installed based on the conda environment. For each package, there will be an
option to skip the installation using that package manager. In this case, the
next package manager will be probed, or the package will not be installed in
case there are no further package managers to probe. If a package is not found
in any of the package managers, the user will be asked whether the installation
should be continued or not. After all packages are selected for installation,
each package will be installed individually. Hence, a failure in the
installation of one package will not prevent other packages from being
installed. Once all the packages are installed, the tests will be executed and
if succesful, the software will be copied to the output directory.

Installing without any tests
++++++++++++++++++++++++++++

In case there are not tests available, you use the ``--skip-tests`` option and
platform migrator will follow the above process for installing the packages and
copy the software over to the output directory, if the installations were
completed succesfully.

