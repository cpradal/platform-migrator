========
Abstract
========

Currently, one of the major problems in software development and maintenance,
specially in academia, is the managing packages across time and systems.
An application developed under a particular package manager using a certain set
of packages does not always work reliably when ported to a different system
or when abandoned for a period of time and picked up again with newer versions
of the packages. In this report, we provide and describe Platform Migrator, a
software that makes it easy to test applications across systems by identifying
various packages in the base system, figuring out their corresponding
equivalents in the new system and testing whether the software works as
expected on the new system. Platform migrator can migrate software written and
setup inside a conda environment to any Linux based system with conda or some
other package manager. The philosophy of platform migrator is to identify a
closure of the required dependencies for the software being migrated using the
conda environment metadata and then use that closure to install the various
dependencies on the target system. This documentation provides comprehensive
details on how to use platform migrator and what it does internally to migrate
software from one system to another. It also contains tutorials and case studies
that can be replicated for better understanding of the process.
