================================
Writing test configuration files
================================

This describes how to write a test configuration file for a package migration.
The test configuration can contain tests and settings for multiple software or
multiple test configuration files can be used for a particular software.

In other words, multiple test configurations files are read by platform
migrator as if they were one file and only those sections which are applicable
to a particular software are used.

There are two main types of sections, software sections and test sections. Test
sections have names starting with ``test_``. All other sections are treated as
software sections. A special secction ``[DEFAULT]`` may be used to create some
default options for each software like package manager names and output
directory location.

Software sections
-----------------

A software section is named after the software being migrated and contains the
following options: ::

    [<software-name>]
    package_managers = <pkg-mgr0>[, <pkg-mgr1>, ...]
    output_dir = /an/absolute/path
    tests = <test0>[, <test1>, ...]

The ``package_managers`` options is a comma separated list of package managers
that will be used for migrating that particular software. These package manager
names must correspond to section names in the package manager configuration
files being used for the migration.

The ``output_dir`` is an absolute path to the location where the software
should be saved.

``tests`` contains a comma separated list of tests for the software. They must
be test section names without the ``test_`` prefix and the test sections must
be in one of the test configuration files being used for the migration.

Test sections
-------------

A test section begins with ``test_`` and contains only one option, ``exec``: ::

    [test_<test0>]
    exec = python some_script.py

    [test_<test1>]
    exec = ~/test-script.sh

The option must be a command that can be executed by the default shell or an
executable shell script. Any paths in the command must be relative to the
directory which contains the software being migrated or an absolute path. So,
``some_script.py`` should be directly inside the software's top level directory
while ``test-script.sh`` is in the home directory.

If the command returns 0 upon execution, the test is considered successful,
otherwise it is considered a failure. The output from the tests is always
printed out on the terminal.
