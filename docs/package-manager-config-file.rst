===========================
Package manager config file
===========================

A package manager is configured in a .ini file. One file can contain multiple
package managers.

Each package manager is a section and must contain the following keys:

    :name: The name of the package manager. This can be any string.
    :exec: The package manager executable. If empty, it defaults to the
        package manager name.
    :install: An install command which takes the package name as the argument.
        Wildcard %p can be used to indicate the position of package name in
        the string. Otherwise, it is assumed that the package name is to be
        added to the end.
    :install_version: An install command which takes package name and version
        as arguments. Wildcards %p and %v can be used in a format string to
        indicate the position of the package and version respectively.
    :search: A search command which takes the search expression an input.
        Wildcard %s can be used to indicate position of the search expression.
        Otherwise, it is assumes that the expression should go at the end.
    :result_fmt: A Python regexp which will match individual lines in the
        output returned by the search command. Wildcards %p and %v must be
        used to indicate the package name and version.

The commands can have the following substitution parameters:

    :%e: The package manager executable defined in the same section. It may
        be used only in the ``search``, ``install`` or ``install_version``
        options. If this is not present in the option, the command will be
        executed as is.
    :%p: The package name. It must be used in the ``install_version`` and
        in ``result_fmt`` otherwise the options are invalid. It may also be
        used in the ``install`` option, otherwise it will be appended to the
        the ``install`` option by default. It is not valid in other options.
    :%s: A search string. It may be used only in the ``search`` option and
        will be appended by defualt if not provided.
    :%v: The package version. It must be used in the ``install_version``
        otherwise it is invalid. It may also be used in the ``result_fmt``
        option. It is not valid in any other option.
