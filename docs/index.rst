.. Platform Migrator documentation master file, created by
   sphinx-quickstart on Sun Mar 18 16:45:28 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Platform Migrator's documentation!
=============================================

Platform migrator is a tool which helps with migrating software from system to
another. Its main aim is to ease the process of installing dependencies
required for the software. It interfaces with various package managers to
install the required dependencies and runs tests to identify whether the
installation was succesful or not.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   abstract
   intro
   cli
   tutorial
   package-manager-config-file
   test-config-file
   case-studies
   internals
   future
   api-doc
