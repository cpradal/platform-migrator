===========================
Comand Line Interface (CLI)
===========================

Command line arguments and options
----------------------------------

On a target system, platform migrator executes in server mode or in migrate
mode. In server mode, it accepts commands to control the HTTP server,
interacts with the base system to receive the software and prepares it for
migration. In migrate mode, it tries to migrate a software that has been
prepared using either the server mode or from a zip file via the ``pack``
and ``unpack`` commands described below.

Additionally ``pack`` and ``unpack`` commands are available that can be used to
pack and unpack a zip file for transfer over email or some means other than the
platform migrator server. The ``pack`` command is run on the base system and it
executes the client side of the server mode, while the ``unpack`` command is
run on the target system to prepare the software for migration. These are
helper commands which can be used as a replacement for the server mode, when it
is not possible to connect the two systems over a network.

The usage for server mode is ::

    platform-migrator server [-h] [--host HOST] [--port PORT] {start,stop,status}

    commands:
      start                Starts a simple http server on specified host and port
      stop                 Stops the currently running server
      status               Prints whether a server is currently running or not

    optional arguments:
      -h, --help           show this help message and exit
      --host HOST          Hostname or IP address to listen on (Default: localhost)
      --port PORT          Port number to listen on (Default: 9000)

The usage for migrate mode is ::

    platform-migrator migrate [-h] [--skip-tests] [--pck-mgr-configs PCK_MGR_CONFIGS] name test_files [test_files ...]

    positional arguments:
      name                  Name of package to migrate
      test_files            One or more test configuration files for migration

    optional arguments:
      -h, --help            show this help message and exit
      --skip-tests          Skip tests but install dependencies and copy files
      --pck-mgr-configs PCK_MGR_CONFIGS
                            Config file for additional package managers

The usage for pack mode is ::

    usage: platform-migrator pack [-h]

    Pack the software in a zip file for emailing

    optional arguments:
      -h, --help  show this help message and exit

The usage for unpack mode is ::

    platform-migrator unpack [-h] zip_file

    Unpack a zip file

    positional arguments:
      zip_file    The zip file to unpack

    optional arguments:
      -h, --help  show this help message and exit

Environment variables
---------------------

Platform migrator uses CONDA_HOME or CONDA_ROOT_DIR environment variables to
determine the location of conda installation. These should point to the top
level directory under which conda is installed. So, if conda is installed in
the home directory of user bob, the value for either variable should be
``/home/bob/anaconda``.
