"""Unit tests for code in migrator.py

Currently, most of it it tested as part of the regression tests
but the get_target_from_search function is tested here. The tests
require manual interaction.
"""

import os
import sys
import unittest


os.chdir(os.path.dirname(os.path.abspath(__file__)) + '/../')
sys.path.insert(0, os.getcwd())

from platform_migrator import migrator


class Test_get_target(unittest.TestCase):

    def test_version(self):
        pcks = [
                {'pck': 'foo', 'ver': '1.2.3'},
                {'pck': 'foo', 'ver': '2.3.4'}
               ]
        choice = migrator.get_target_from_search(pcks)
        if choice is not None:
            self.assertIn(choice, pcks)

    def test_no_version(self):
        pcks = [
                {'pck': 'foo'},
                {'pck': 'bar'}
               ]
        choice = migrator.get_target_from_search(pcks)
        if choice is not None:
            self.assertIn(choice, pcks)

    def test_dups(self):
        pcks = [
                {'pck': 'foo'},
                {'pck': 'foo'}
               ]
        choice = migrator.get_target_from_search(pcks)
        if choice is not None:
            self.assertIn(choice, pcks)


if __name__ == '__main__':
    unittest.main(exit=False)
