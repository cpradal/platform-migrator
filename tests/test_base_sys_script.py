"""Tests for the base system script

The tests takes the CONDA_ROOT_DIR environment variable to determine
the location of conda installation. If it is not set, it assumes that
the current directory is the conda root directory. The tests must be
run from an active conda environment.
"""


import io
import os
import shutil
import sys
import unittest
import zipfile


os.chdir(os.path.dirname(os.path.abspath(__file__)) + '/../')
sys.path.insert(0, os.getcwd())

from platform_migrator import base_sys_script

class Test_base_sys_script(unittest.TestCase):

    def setUp(self):
        self.cwd = os.getcwd()

    def tearDown(self):
        if os.path.isdir('/tmp/tests'):
            shutil.rmtree('/tmp/tests/')
        if os.path.exists('/tmp/env.yml'):
            os.remove('/tmp/env.yml')
        if os.path.exists('/tmp/min-deps.json'):
            os.remove('/tmp/min-deps.json')
        if os.path.exists('..zip'):
            os.remove('..zip')
        os.chdir(self.cwd)

    def test_activate_and_get_conda_env(self):
        os.chdir(os.getenv('CONDA_ROOT_DIR', '.') + '/bin/')
        res = base_sys_script.get_conda_env('root')
        if not res[0]:
            print('Stdout of process is:\n%s\n\nStderr of process is:\n%s',
                  res[1], res[2])
        self.assertTrue(res[0])

    def test_get_conda_env(self):
        os.chdir(os.getenv('CONDA_ROOT_DIR', '.') + '/bin/')
        res = base_sys_script.get_conda_env()
        if not res[0]:
            print('Stdout of process is:\n%s\n\nStderr of process is:\n%s',
                  res[1], res[2])
        self.assertTrue(res[0])

    def test_get_conda_min_deps(self):
        os.chdir(os.getenv('CONDA_ROOT_DIR', '.') + '/bin/')
        res = base_sys_script.get_conda_min_deps(os.getenv('CONDA_ENV', 'root'))
        self.assertTrue(res)

    def test_create_zip_file(self):
        zip_file = base_sys_script.create_zip_file('tests/', quiet=True)
        self.assertIsInstance(zip_file, bytes)
        with zipfile.ZipFile(io.BytesIO(zip_file), mode='r') as zf:
            self.assertIsInstance(zf, zipfile.ZipFile)
            zf.extractall(path='/tmp')
        self.assertTrue(os.path.isdir('/tmp/tests'))
        self.assertTrue(os.path.exists('/tmp/tests/test_base_sys_script.py'))
        self.assertTrue(os.path.isdir('/tmp/tests/basic-migration'))

    def test_create_zip_pack(self):
        zip_file = base_sys_script.create_zip_file('tests/', env_yml='foo',
                                                   min_deps='bar', quiet=True)
        self.assertIsInstance(zip_file, bytes)
        with zipfile.ZipFile(io.BytesIO(zip_file), mode='r') as zf:
            self.assertIsInstance(zf, zipfile.ZipFile)
            zf.extractall(path='/tmp')
        self.assertTrue(os.path.isdir('/tmp/tests'))
        self.assertTrue(os.path.isfile('/tmp/env.yml'))
        self.assertTrue(os.path.isfile('/tmp/min-deps.json'))
        self.assertTrue(os.path.exists('/tmp/tests/test_base_sys_script.py'))
        self.assertTrue(os.path.isdir('/tmp/tests/basic-migration'))


if __name__ == '__main__':
    unittest.main(exit=False)
